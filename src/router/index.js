import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/views/Home'
import Layout from '@/views/Layout'
import Project from '@/views/backend/Project'
import Document from '@/views/backend/Document'
import WorkBench from '@/views/backend/WorkBench'
Vue.use(Router)

export default new Router({
  mode:"history",
  linkActiveClass:"activeClass",
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },  {
      path: '/login',
      name: 'login',
      component: ()=>import("@/views/Login")
    },{
      path: '/management',
      name: 'management',
      component: Layout,
      children:[{
        path: '/project',
        name: 'project',
        component: Project,
      },{
        path: '/doc',
        name: 'doc',
        component: Document,
      },{
        path: '/workBench',
        name: 'workBench',
        component: WorkBench,
      }]
    },{
      path:"*",
      redirect:"/"
    }
  ]
})
