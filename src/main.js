import Vue from 'vue'
import App from './App'
import router from './router'
import  './assets/css/app.css'
import Util from './util/util'
Vue.config.productionTip = false
// 这里会执行Util的install方法。
Vue.use(Util)
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
